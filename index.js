var express = require('express')
var cowsay = require('cowsay')

var ws = express()

ws.get('/:greeting', (req, res) => {
var user = req.get('ADFS_LOGIN')
var text = user + ' - ' + req.params.greeting
var response = `<pre style="color: yellow">${cowsay.say({text})}</pre>`
res.send(response)
})

ws.listen(8080)
